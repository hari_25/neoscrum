"use strict";

const bcrypt = require("bcryptjs");

const UserModel = require("../src/Models/User");

module.exports = async () => {
    console.log(`Server is running on http://localhost:${process.env.PORT}`);

    const userCount = await UserModel.countDocuments({ is_admin: true });

    if (userCount === 0) {
        const userData = {
            name: "Admin",
            email: "admin@admin.com",
            image: "admin.jpg",
            is_admin: true,
            password: await bcrypt.hash("123456", 10),
        };

        const user = await new UserModel(userData).save();
    }
};
