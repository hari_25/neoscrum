"use strict";

const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

module.exports = mongoose.model(
    "User",
    new mongoose.Schema(
        {
            name: {
                type: String,
                trim: true,
                required: true,
            },
            email: {
                type: String,
                trim: true,
                required: true,
                unique: true,
            },
            password: {
                type: String,
                required: true,
            },
            // image: {
            //     type: String,
            //     required: true,
            //     get: imageURL,
            // },
            is_admin: {
                type: Boolean,
                default: false,
            },
            token: {
                type: String,
            },
        },
        {
            timestamps: true,
            versionKey: false,
            toJSON: {
                transform(doc, ret) {
                    delete ret.password;
                    delete ret.id;
                    delete ret.createdAt;
                    delete ret.updatedAt;
                },
                getters: true,
            },
        },
    ).plugin(mongoosePaginate),
);

function imageURL(image) {
    return `${process.env.IMAGE_URL}${image}`;
}
