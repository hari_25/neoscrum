"use strict";

const mongoose = require("mongoose");

module.exports = mongoose.model(
    "Feedback",
    new mongoose.Schema(
        {
            senderEmpId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User",
                alias: "sender",
                required: true,
            },
            receiverEmpId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User",
                alias: "receiver",
                required: true,
            },
            comment: {
                type: String,
                default: null,
            },
            rate: {
                type: Number,
                default: null,
            },
            monthYear: {
                type: String,
                required: true,
            },
        },
        {
            timestamps: true,
            versionKey: false,
            toJSON: {
                transform(doc, ret) {
                    delete ret.id;
                    delete ret.senderEmpId;
                    delete ret.receiverEmpId;
                    delete ret.createdAt;
                    delete ret.updatedAt;
                },
                getters: true,
            },
        },
    ),
);
