"use strict";

const express = require("express");

const { signin, logout } = require("../Controllers/AuthController");

const { validator } = require("../Middlewares/Validator");
const { login, register } = require("../../helpers/Schemas");
const { createUser } = require("../Controllers/AdminController");

const router = express.Router();

router.post("/register", validator(register), createUser);
router.post("/login", validator(login), signin);
router.post("/logout", logout);

module.exports = router;
