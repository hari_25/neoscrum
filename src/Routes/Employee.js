"use strict";

const express = require("express");

const { auth, employee } = require("../Middlewares/Auth");
const { list, feedbackSubmit, feedbackRecieved } = require("../Controllers/EmployeeController");
const { feedaback } = require("../../helpers/Schemas");
const { validator } = require("../Middlewares/Validator");

const router = express.Router();

router.post("/list", [auth, employee], list);
router.post("/feedback", [auth, employee], validator(feedaback), feedbackSubmit);
router.post("/feedback/recieved", [auth, employee], feedbackRecieved);

module.exports = router;
