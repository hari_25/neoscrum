"use strict";

const express = require("express");

const { auth, admin } = require("../Middlewares/Auth");
const { register } = require("../../helpers/Schemas");
const { validator } = require("../Middlewares/Validator");
const {
    userList,
    createUser,
    assignEmployee,
} = require("../Controllers/AdminController");

const router = express.Router();

router.post("/get_user", [auth, admin], userList);
router.post("/create_user", [auth, admin], validator(register), createUser);
router.post("/assign_employee", [auth, admin], assignEmployee);

module.exports = router;
