"use strict";

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const UserModel = require("../Models/User");
const { LOGIN_SUCCESS, INVALID_CREDENTIAL } = require("../Lang/EN/Auth");

const { apiResponse, errorResponse } = require("../../helpers/ApiResponse");

module.exports.signin = async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await UserModel.findOne({ email: email });

        if (user && (await bcrypt.compare(password, user.password))) {
            const token = jwt.sign(
                { _id: user._id, is_admin: user.is_admin },
                process.env.SECRET,
            );

            user.token = token;
            user._id = null;

            res.cookie("token", token, { expire: new Date() + 9999 });

            return apiResponse(res, LOGIN_SUCCESS, 200, user);
        } else {
            return apiResponse(res, INVALID_CREDENTIAL, 401);
        }
    } catch (err) {
        console.log(err);
        return errorResponse(res, err);
    }
};

module.exports.logout = (req, res) => {
    res.clearCookie("token");
    const msg = "User logout successfully";

    return apiResponse(res, msg, 200);
};
