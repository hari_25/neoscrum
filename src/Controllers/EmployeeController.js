"use strict";

const FeedbackModel = require("../Models/Feedback");

const { apiResponse, errorResponse } = require("../../helpers/ApiResponse");

exports.list = async (req, res) => {
    try {
        const feedbacks = await FeedbackModel.find({
            senderEmpId: req.params._id,
            comment: null,
        }).populate(["receiverEmpId", "senderEmpId"]);
        const msg = feedbacks.length
            ? "User found"
            : "No user available for review!";

        return apiResponse(res, msg, 200, feedbacks);
    } catch (err) {
        return errorResponse(res, err);
    }
};

exports.feedbackRecieved = async (req, res) => {
    try {
        const feedbacks = await FeedbackModel.find({
            receiverEmpId: req.params._id,
            comment: { $ne: null },
        });
        const msg = feedbacks.length ? "Feedback found" : "No feedback found!";

        return apiResponse(res, msg, 200, feedbacks);
    } catch (err) {
        return errorResponse(res, err);
    }
};

exports.feedbackSubmit = async (req, res) => {
    try {
        const query = { _id: req.body.id };
        const update = {
            $set: { comment: req.body.comment, rate: req.body.rate },
        };

        const options = { upsert: true };
        const msg = "Feedback submited successfully";

        await FeedbackModel.findOneAndUpdate(query, update, options);

        return apiResponse(res, msg, 200);
    } catch (err) {
        return errorResponse(res, err);
    }
};
