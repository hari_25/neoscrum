"use strict";

const bcrypt = require("bcryptjs");

const UserModel = require("../Models/User");
const FeedbackModel = require("../Models/Feedback");

const { single } = require("../../helpers/FileUpload");
const { apiResponse, errorResponse } = require("../../helpers/ApiResponse");

exports.createUser = async (req, res) => {
    try {
        const userCount = await UserModel.countDocuments({
            email: req.body.email,
        });

        if (userCount) {
            const msg = "Email already exist";

            return apiResponse(res, msg, 400);
        }

        req.body.password = await bcrypt.hash(req.body.password, 10);
        req.body.image = (await single(req, res, "image")) || null;

        const msg = "User registered successfully";
        const user = await new UserModel(req.body).save();

        return apiResponse(res, msg, 200, user);
    } catch (err) {
        console.log("err", err);
        return errorResponse(res, err);
    }
};

exports.assignEmployee = async (req, res) => {
    const date = new Date();
    const monthYear = date.getMonth() + 1 + "/" + date.getFullYear();

    try {
        const feedbackCount = await FeedbackModel.countDocuments({
            monthYear: monthYear,
        });

        if (feedbackCount) {
            const msg = "Already assigned!";

            return apiResponse(res, msg, 200);
        }

        const msg = "Assigned user successfully";

        const users = await UserModel.find({ is_admin: false });

        if (users.length < 5) {
            const msg = "Please add at least 4 users first!";

            return apiResponse(res, msg, 400);
        }

        let userIds = users.map((item) => {
            return item._id;
        });

        let tempIds = userIds;

        for (let i = 0; i < userIds.length; i++) {
            let senderEmpId = userIds[i];
            tempIds = tempIds.filter((e) => e !== senderEmpId);
            let selectedIds = getRandom(tempIds, 3);
            tempIds = userIds;

            for (let j = 0; j < selectedIds.length; j++) {
                const assignedUsers = {
                    senderEmpId: senderEmpId,
                    receiverEmpId: selectedIds[j],
                    monthYear: monthYear,
                };

                await new FeedbackModel(assignedUsers).save();
            }
        }

        return apiResponse(res, msg, 200);
    } catch (err) {
        return errorResponse(res, err);
    }
};

exports.userList = async (req, res) => {
    try {
        const msg = "User found successfully";

        const users = await UserModel.paginate(
            { is_admin: false },
            { page: req.body.page, limit: req.body.limit },
        );

        return apiResponse(res, msg, 200, users);
    } catch (err) {
        console.log("err", err);
        return errorResponse(res, err);
    }
};

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}
