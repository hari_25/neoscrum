"use strict";

const { apiResponse } = require("../../helpers/ApiResponse");

module.exports.validator = (schema) => {
    return (req, res, next) => {
        const { error } = schema.validate(req.body);

        if (error !== undefined) {
            return apiResponse(res, error.details[0].message, 400);
        }

        return next();
    };
};
