"use strict";

const jwt = require("jsonwebtoken");

const UserModel = require("../Models/User");

const { apiResponse, errorResponse } = require("../../helpers/ApiResponse");

exports.auth = async (req, res, next) => {
    const token =
        req.body.token || req.query.token || req.headers["authorization"];

    if (!token) {
        const msg = "Token is required";
        return apiResponse(res, msg, 401);
    }

    try {
        const decodeData = jwt.verify(token, process.env.SECRET);

        const msg = "Access Denied!";
        const userCount = await UserModel.countDocuments({
            _id: decodeData._id,
        });

        if (userCount) {
            req.params = decodeData;
            return next();
        }

        return apiResponse(res, msg, 401);
    } catch (err) {
        const msg = "Invalid Token";
        return errorResponse(res, msg);
    }
};

exports.admin = (req, res, next) => {
    try {
        if (req.params.is_admin) {
            return next();
        }

        return apiResponse(res, "Access denied!", 401);
    } catch (err) {
        return errorResponse(res, err);
    }
};

exports.employee = (req, res, next) => {
    try {
        if (req.params.is_admin) {
            return apiResponse(res, "Access denied!", 401);
        }

        return next();
    } catch (err) {
        return errorResponse(res, err);
    }
};
