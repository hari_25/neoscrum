/**
 * @swagger
 * components:
 *   schemas:
 *     Auth:
 *       type: object
 *       required:
 *         - email
 *         - password
 *       properties:
 *         email:
 *           type: string
 *         password:
 *           type: string
 *       example:
 *         email: admin@admin.com
 *         password: "123456"
 *     CreateUser:
 *       type: object
 *       required:
 *         - email
 *         - password
 *          -auth
 *       properties:
 *         email:
 *           type: string
 *         password:
 *           type: string
 *         auth:
 *           type: string
 *       example:
 *         email: admin@admin.com
 *         password: "123456"
 */

/**
 * @swagger
 * tags:
 *   name: Login
 *   description: Login API
 */

/**
 * @swagger
 * /login:
 *   post:
 *     summary: Returns loggedin user with token
 *     tags: [Login]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Auth'
 *     responses:
 *       200:
 *         description: Login details
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Auth'
 * 
 */

/**
 * @swagger
 * tags:
 *   name: CreateUser
 *   description: CreateUser API
 */

/**
 * @swagger
 * /admin/create_user:
 *   post:
 *     summary: Returns user list
 *     tags: [CreateUser]
 *     requestBody:
 *       required: true
 *       content:
 *         form-data:
 *           schema:
 *             $ref: '#/components/schemas/CreateUser'
 *       response:
 *         200:
 *         description: user list
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/CreateUser'
 * 
 */