"use strict";

require("dotenv").config();

const path = require("path");
const cors = require("cors");
const express = require("express");
const cookieParser = require("cookie-parser");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
const fileUpload = require("express-fileupload");

require("./config/mongoConnection");

const UserSeeder = require("./seeder/UserSeeder");
const AuthRoute = require("./src/Routes/Auth");
const AdminRoute = require("./src/Routes/Admin");
const EmployeeRoute = require("./src/Routes/Employee");

const app = express();
const PORT = process.env.PORT || 5000;

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Library API",
            version: "1.0.0",
            description: "A simple Express Library API",
        },
        servers: [
            {
                url: "http://localhost:7000/api/v1",
            },
        ],
    },
    apis: ["./swagger/api_doc.js"],
};

const specs = swaggerJsDoc(options);

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

app.use(
    fileUpload({
        createParentPath: true,
    }),
);

app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "/")));

app.use("/api/v1", AuthRoute);
app.use("/api/v1/admin", AdminRoute);
app.use("/api/v1/employee", EmployeeRoute);

app.listen(PORT, UserSeeder);
