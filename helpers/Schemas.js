"use strict";

const Joi = require("joi");

const Schemas = {
    login: Joi.object().keys({
        email: Joi.string().email().lowercase().required().messages({
            "string.base": "Email should be string",
            "string.email": "Email should be in format.",
            "string.lowercase": "Email sholud be in lowercase",
            "any.required": "Email is required",
        }),
        password: Joi.string().min(6).required().empty().messages({
            "string.base": "Password should be string",
            "string.min": "Password must contain 6 characters.",
            "string.empty": "Password is required!!",
            "any.required": "Password is required",
        }),
    }),
    register: Joi.object().keys({
        name: Joi.string().empty().min(3).required().messages({
            "string.base": "Name should be string",
            "string.empty": "Name is required!!",
            "string.min": "Name must contain 3 characters.",
            "any.required": "Name is required",
        }),
        email: Joi.string().email().lowercase().required().messages({
            "string.base": "Email should be string",
            "string.email": "Email should be in format.",
            "string.lowercase": "Email sholud be in lowercase",
            "any.required": "Email is required",
        }),
        password: Joi.string().min(6).required().messages({
            "string.base": "Password should be string",
            "string.min": "Password must contain 6 characters.",
            "any.required": "Password is required",
        }),
    }),
    feedaback: Joi.object().keys({
        comment: Joi.string().empty().min(10).required().messages({
            "string.base": "Comment should be string",
            "string.empty": "Comment is required!!",
            "string.min": "Comment must contain 10 characters.",
            "any.required": "Comment is required",
        }),
        id: Joi.string().required().messages({
            "string.base": "ID should be string",
            "any.required": "ID is required",
        }),
        rate: Joi.number().required().messages({
            "string.number": "Rate should be string",
            "any.required": "Rate is required",
        }),
    }),
};

module.exports = Schemas;
