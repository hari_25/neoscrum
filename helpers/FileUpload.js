"use strict";

const apiResponse = require("./ApiResponse");

module.exports.single = (req, res, key = "") => {
    try {
        if (req.files && req.files[key]) {
            const file = req.files[key];
            file.name = Date.now() + "." + file.name.split(".").pop();
            file.mv(process.env.UPLOAD_PATH + file.name);

            return file.name;
        } else {
            return null;
        }
    } catch (err) {
        return apiResponse.apiResponse(res, "Error to upload image", 400);
    }
};
