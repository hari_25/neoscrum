"use strict";

exports.apiResponse = function (res, msg, statusCode = 200, data = []) {
    const response = {
        message: msg,
        data: data,
    };

    return res.status(statusCode).json(response);
};

exports.errorResponse = function (res, msg) {
    const response = {
        message: msg,
        data: [],
    };

    return res.status(500).json(response);
};
